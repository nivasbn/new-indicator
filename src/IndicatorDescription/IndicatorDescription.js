import React from "react";
import "../Indicator/stylee.css";

const IndicatorDescription = (props) => {
  //Change=Increase, Intent=Neutral, State=Enabled
  //Change=Increase, Intent=Bad, State=Enabled
  //Change=Increase, Intent=Good, State=Enabled

  let change = props.change;
  let intent = props.intent;
  let state = props.state;

  if (state === "Enabled") {
    if (change === "increase") {
      if (intent === "good") {
        intent = "description-good description-indicator";
      } else if (intent === "bad") {
        intent = "description-bad description-indicator";
      } else if (intent === "neutral") {
        intent = "description-neutral description-indicator";
      }
      return (
        <div>
          <span className={intent}> an {props.change} of x%</span>
        </div>
      );
    } else if (change === "decrease") {
      if (intent === "good") {
        intent = "description-good description-indicator";
      } else if (intent === "bad") {
        intent = "description-bad description-indicator";
      } else if (intent === "neutral") {
        intent = "description-neutral description-indicator";
      }
      return (
        <div>
          <span className={intent}> a {props.change} of x%</span>
        </div>
      );
    } else if (change === "none") {
      if (intent === "good") {
        intent = "description-good description-indicator";
      } else if (intent === "bad") {
        intent = "description-bad description-indicator";
      } else if (intent === "neutral") {
        intent = "description-neutral description-indicator";
      }
      return (
        <div>
          <span className={intent}>the same</span>
        </div>
      );
    }
  } else if (state === "No Data") {
    console.log("none");
  }
};

export default IndicatorDescription;

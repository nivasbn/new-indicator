// import React, { Component } from 'react'
// import IndicatorDescription from "./IndicatorDescription"

// export class ClassIndicatorDescription extends Component {
//   render() {
//     return (
//       <div>
//       <IndicatorDescription change="decrease" intent="good" state="No Data"  />
//       </div>
//     )
//   }
// }

// export default ClassIndicatorDescription

import React from 'react'
import IndicatorDescription from "./IndicatorDescription";

function ClassIndicatorDescription() {
  return (
      <div>
      <IndicatorDescription change="decrease" intent="neutral" state="Enabled"  />
      </div>
    )
}

export default ClassIndicatorDescription



  //Change=Increase, Intent=Neutral, State=Enabled
  //Change=Increase, Intent=Bad, State=Enabled
  //Change=Increase, Intent=Good, State=Enabled

  //state=Enabled,No Data
import React from "react";
import "./AtomicComp.css";
import Vector from "../icons/Vector.svg";
import TabComp from "./TabComp";
import IndicatorComp from "./IndicatorComp";

function AtomicComp() {
  return (
    <div className="container">
      <div className="header">
        <img src={Vector} alt="vector" />
        <p>a card title in 18px, SemiBold</p>
      </div>

      <div className="text-header">
        <p>more detailed explanation matching the card body in 14px</p>
      </div>

     <TabComp/>
    <IndicatorComp/>


      <p className="date-indicator"> Oct 15th-Oct 21st 2022</p>
      
      
      <div className="indicator-text">
      <p>the thing you counted in 14px, which is <span>an increase of 339,600.00%</span> from the last hour</p>
      </div>


      <div className="graph-container">    </div>

<button className="button">button</button>

    </div>
  );
}

export default AtomicComp;

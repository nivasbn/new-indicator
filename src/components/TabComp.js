import React from 'react'
import "./AtomicComp.css"
function TabComp() {
  return (
    <div className="tabs-container">
      <div className="hour-container">
        <p>hour</p>
      </div>
      <div className="day-container">
        <p>day</p>
      </div>
      <div className="week-container">
        <p>week</p>
      </div>
      <div className="month-container">
        <p>month</p>
      </div>
      <div className="year-container">
        <p>year</p>
      </div>
    </div>
  );
}

export default TabComp
import React from 'react'
import "./AtomicComp.css"
import right from "../icons/right.svg";
import indicator from "../icons/indicator.svg";
function IndicatorComp() {

    
  return (
    <div className="indicator-container">
      <img className="right" src={right} alt="right" />
      <p>27</p>
      <img className="indicator" src={indicator} alt="indicator" />
      <img className="left" src={right} alt="left" />
    </div>
  );
}

export default IndicatorComp
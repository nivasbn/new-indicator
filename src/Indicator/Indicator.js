import React from 'react'
import Data from "./data.json"
import "./stylee.css"

function Indicator() {

  return (
    <div>
      
      {
          Data.map(data=>{
              let value=data.value
              let unit=data.unit
              let determiner=data.determiner

              var intent=[]
                       
            if (value>0) {
             
             intent="good data-indicator"
            
            } else if(value<0) {
              
                 intent = "bad data-indicator";
            } else{
              
                 intent = "neutral data-indicator";
            }

              return (
                <div className="box">
                  {determiner} <b>{value}</b> <span className=  {intent} ></span>  {unit}
                
                </div>
              );
          })
      }
    </div>
  );
}

export default Indicator

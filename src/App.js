import logo from './logo.svg';
import './App.css';
import GetYesterdayDate from './getYesterdayDate';
import AtomicComp from './components/AtomicComp';
import CopyComp from './CopyComp';
import Indicator from './Indicator/Indicator';
import NewIndicator from './new/NewIndicator';
import ClassIndicator from './new/ClassIndicator';
import CardValues from './CardValues/CardValues';
import IndicatorDescription from "./IndicatorDescription/ClassIndicatorDescription"
import DateComp from "./Date/ClassDate"


function App() {

 return (
   <div className="App">
     <IndicatorDescription />
   </div>
 );
}

export default App;

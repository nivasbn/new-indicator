import React, { Component } from 'react'
import NewIndicator from './NewIndicator'

export class ClassIndicator extends Component {
  render() {
    return (
      <div>
        <NewIndicator change="none" intent="good" state="Enabled"/>
      </div>
    );
  }
}

export default ClassIndicator

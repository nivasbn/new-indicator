import React from "react"

const NewIndicator=props=>{

    let change_indicator=props.change
    let intent=props.intent
   let state=props.state
   
   if (state==="Enabled") {
        if (change_indicator === "increase") {
          if (intent === "good") {
            intent = "increase-good data-indicator";
          } else if (intent === "bad") {
            intent = "increase-bad data-indicator";
          } else if (intent === "neutral") {
            intent = "increase-neutral data-indicator";
          }
        } else if (change_indicator === "decrease") {
          if (intent === "good") {
            intent = "decrease-good data-indicator";
          } else if (intent === "bad") {
            intent = "decrease-bad data-indicator";
          } else if (intent === "neutral") {
            intent = "decrease-neutral data-indicator";
          }
        } else if (change_indicator === "none") {
          if (intent === "good") {
            intent = "none-good data-indicator";
          } else if (intent === "bad") {
            intent = "none-bad data-indicator";
          } else if (intent === "neutral") {
            intent = "none-neutral data-indicator";
          }
        } else if (change_indicator === "status") {
          if (intent === "good") {
            intent = "status-good data-indicator";
          } else if (intent === "bad") {
            intent = "status-bad data-indicator";
          } else if (intent === "neutral") {
            intent = "status-neutral data-indicator";
          }
        } 
   } else if(state==="no-data") {
       
   }else if(state==="skeleton"){
       intent="skeleton data-indicator"
   }


   

    return (
      <div>
         <span className=  {intent} ></span>
      </div>
    );}
export default NewIndicator




import React from 'react'

function getYesterdayDate() {
      const timeStamp = new Date().getTime();
      const yesterdayTimeStamp = timeStamp - 24 * 60 * 60 * 1000;
      const yesterdayDate = new Date(yesterdayTimeStamp);
  return new Date(new Date().getTime() - 24*60*60*1000);
 
}
 console.log(getYesterdayDate());
export default getYesterdayDate